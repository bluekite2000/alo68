 from app import app, db
from flask import make_response, redirect, render_template, request, session, url_for,Response
from app.models import *    
from sqlalchemy.exc import IntegrityError
import os
import stripe
import plivo
import sys
from random import randint
import math
app.config.from_pyfile('keys.cfg')

stripe_keys = {
    'stripe_secret_key': app.config['STRIPE_SECRET_KEY'],
    'stripe_publishable_key': app.config['STRIPE_PUBLISHABLE_KEY']
}
plivo_keys = {
    'plivo_secret_key': app.config['PLIVO_SECRET_KEY'],
    'plivo_publishable_key': app.config['PLIVO_PUBLISHABLE_KEY']
}
stripe.api_key = stripe_keys['stripe_secret_key']


@app.route('/')
def index():
  return render_template('index.html', key=stripe_keys['stripe_publishable_key'])
 

 
 
@app.route('/chargebronze', methods=['POST'])
def chargebronze():
  amount = 2000
  customer = stripe.Customer.create(
        email='customer@example.com',
        card=request.form['stripeToken']
  )
 
  charge = stripe.Charge.create(
        customer=customer.id,
        amount=amount,
        currency='usd',
        description='Bronze Charge'
  )
  pinnum=randint(10000000, 99999999)
  new_pin = Pin(pinnum,20.00)
  db.session.add(new_pin)
  db.session.commit()
     
  return render_template('charge.html', amount=amount,pinnum=pinnum)
  
  
@app.route('/chargesilver', methods=['POST'])
def chargesilver():
  amount = 5000
  customer = stripe.Customer.create(
        email='customer@example.com',
        card=request.form['stripeToken']
  )
 
  charge = stripe.Charge.create(
        customer=customer.id,
        amount=amount,
        currency='usd',
        description='Silver Charge'
  )
  pinnum=randint(10000000, 99999999)
  new_pin = Pin(pinnum,50.00)
  db.session.add(new_pin)
  db.session.commit()
  return render_template('charge.html', amount=amount,pinnum=pinnum)
  
  
@app.route('/chargegold', methods=['POST'])
def chargegold():
  amount = 10000
  customer = stripe.Customer.create(
        email='customer@example.com',
        card=request.form['stripeToken']
  )
 
  charge = stripe.Charge.create(
        customer=customer.id,
        amount=amount,
        currency='usd',
        description='Gold Charge'
  )
  pinnum=randint(10000000, 99999999)
  new_pin = Pin(pinnum,100.00)
  db.session.add(new_pin)
  db.session.commit()
       
  return render_template('charge.html', amount=amount,pinnum=pinnum)




def exit_sequence(msg="Oops! There was an error!"):
    response = plivo.XML.Response()
    response.addSpeak("We did not receive a valid response. We will hangup now.")
    response.addHangup()
    return Response(str(response), mimetype='text/xml')


    

@app.route('/pinmenu', methods=['POST', 'GET'])
def pinmenu():
    response = plivo.XML.Response()
    response.addSpeak(body="Hello, welcome to alo68 calling service ! Enter your 8 digit pin now.")
    response.addWait(length=1)

    absolute_action_url = url_for('pinmenu_handler', _external=True)
    getDigits = response.addGetDigits(action=absolute_action_url, method='POST',
                                timeout=4,  retries=1)


    response.add(getDigits)

    return Response(str(response), mimetype='text/xml')

@app.route('/pinmenu_handler', methods=['POST',])
def pinmenu_handler():
    post_args = request.form
    #print post_args
    #print request.data
    response = plivo.XML.Response()
    input_digit = post_args.get('Digits', None)

    u = Pin.query.filter_by(pinnumber=int(input_digit)).first()

    if u is None:
        absolute_action_url = url_for('pinmenu_handler', _external=True)
        getDigits = response.addGetDigits(action=absolute_action_url,
                                            method='POST',
                                            timeout=10, 
                                            retries=1)
        getDigits.addWait(length=1)
        getDigits.addSpeak("Wrong pin entered. Try again!")
        response.add(getDigits)
        return Response(str(response), mimetype='text/xml')

    else:
        absolute_action_url = url_for('vnnumbermenu', _external=True,
                                          **{
                                             'pinnumber': str(u.pinnumber)})
        response.addRedirect(body=absolute_action_url, method='POST')
        return Response(str(response), mimetype='text/xml')
            
            
           
  



@app.route('/vnnumbermenu', methods=['POST', 'GET'])
def vnnumbermenu():
    response = plivo.XML.Response()
    input_num=request.args.get('pinnumber', None)
    
    absolute_action_url = url_for('numbermenu_handler', _external=True,
                                            **{
                                             'pinnumber': input_num})

    getDigits = response.addGetDigits(action=absolute_action_url, method='POST',
                                timeout=4,  retries=1)

    u = Pin.query.filter_by(pinnumber=int(input_num)).first()
    getDigits.addSpeak(body="You have %.2f dollars remaining" %u.pinvalue)

  
    getDigits.addSpeak(body="Enter the phone number you want to call now.")
    response.add(getDigits)

    return Response(str(response), mimetype='text/xml')
    
    
    
@app.route('/numbermenu_handler', methods=['POST',])
def numbermenu_handler():
    post_args = request.form

    response = plivo.XML.Response()
    input_num=request.args.get('pinnumber', None)
    input_digit = post_args.get('Digits', None)

    if len(input_digit)<11:
        absolute_action_url = url_for('numbermenu_handler', _external=True,
                                            **{
                                             'pinnumber': input_num})

        getDigits = response.addGetDigits(action=absolute_action_url,
                                            method='POST',
                                            timeout=10, 
                                            retries=1)
        getDigits.addWait(length=1)
        getDigits.addSpeak("You entered an invalid number. Try again!")
        response.add(getDigits)
        return Response(str(response), mimetype='text/xml')

    else:
        #pinnumber=request.args.get('pinnumber', None)
        pinobject = Pin.query.filter_by(pinnumber=int(input_num)).first()
        #how to get call_uuid???
        call_uuid= post_args.get('CallUUID', None)
        newcallobject = CallLog(call_uuid,pinobject)
        db.session.add(newcallobject)
        db.session.commit()   
        
        absolute_callback_url = url_for('getcalldetails', _external=True)

        response.addDial(callbackUrl=absolute_callback_url ,callbackMethod="POST").addNumber(input_digit)
        return Response(str(response), mimetype='text/xml')
            


            
@app.route('/getcalldetails', methods=['POST'])
def getcalldetails(): #This function needs to be called by the callBackUrl in the dial XML
    post_args = request.form
    #p = plivo.RestAPI(plivo_keys[plivo_publishable_key], plivo_keys[plivo_secret_key])
    dialAction = post_args.get('DialAction','')#this works
    response = plivo.XML.Response()
    if dialAction == 'hangup':
        call_uuid= post_args.get('CallUUID', None)
        callobject = CallLog.query.filter_by(call_uuid=call_uuid).first()
       
    	callobject.from_number=post_args.get('DialBLegFrom','')
        callobject.to_number=post_args.get('DialBLegTo','')
        totalcallduration=float(post_args.get('DialBLegDuration',''))
    	callobject.call_duration=math.ceil(totalcallduration/60)#in minute

    	callobject.total_amount=.1*callobject.call_duration#in usd
        
        pinobject=callobject.pin
        pinobject.pinvalue=pinobject.pinvalue-callobject.total_amount
        db.session.commit()
    return Response(str(response), mimetype='text/xml')



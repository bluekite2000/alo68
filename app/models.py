from app import db

class Pin(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    pinnumber = db.Column(db.Integer, unique=True, nullable=False)

    pinvalue = db.Column(db.Float, unique=False, nullable=False)

    callogs = db.relationship('CallLog', backref='pin',
                                lazy='dynamic')
    def __init__(self, pinnumber=None,pinvalue=None):
        self.pinnumber= pinnumber
        self.pinvalue= pinvalue
     

    def __repr__(self):
        return '<Pin %r>' % (self.pinnumber)


class CallLog(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    call_uuid=db.Column(db.String(), unique=True)
    pin_id= db.Column(db.Integer, db.ForeignKey('pin.id'))
    from_number=db.Column(db.String(), unique=False)
    to_number=db.Column(db.String(), unique=False)
    call_duration=db.Column(db.Float, unique=False)
    total_amount=db.Column(db.Float, unique=False)
    def __init__(self, call_uuid=None,pin=None):
        self.call_uuid= call_uuid
        self.pin= pin
    def __repr__(self):
        return '<call_uuid %r>' % (self.call_uuid)